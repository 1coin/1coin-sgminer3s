#ifndef THREES_H
#define THREES_H

#include "miner.h"

extern int threes_test(unsigned char *pdata, const unsigned char *ptarget,
			uint32_t nonce);
extern void threes_regenhash(struct work *work);

#endif /* THREES_H */
